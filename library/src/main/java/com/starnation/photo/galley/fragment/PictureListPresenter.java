package com.starnation.photo.galley.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import com.starnation.android.os.Bundles;
import com.starnation.photo.crop.Crop;
import com.starnation.photo.galley.Galley;
import com.starnation.photo.galley.model.Picture;
import com.starnation.photo.galley.model.PictureList;
import com.starnation.util.validator.InvalidException;
import com.starnation.widget.recyclerview.holder.selector.MultiSelector;
import com.starnation.widget.recyclerview.holder.selector.SingleSelector;

import java.util.ArrayList;

import rx.functions.Action1;

/*
 * @author lsh
 * @since 2017. 3. 21.
*/
public class PictureListPresenter {

    //======================================================================
    // Variables
    //======================================================================

    PictureList mList = new PictureList();

    MultiSelector mMultiSelector;

    Fragment mFragment;

    //======================================================================
    // Constructor
    //======================================================================

    public PictureListPresenter(Fragment fragment) {
        mFragment = fragment;
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public void onCreate() {
        if (isMultiChoiceMode() == true) {
            mMultiSelector = new MultiSelector();
        } else {
            mMultiSelector = new SingleSelector();
        }
    }

    public void onDestroy() {
        mFragment = null;
    }

    public boolean isMultiChoiceMode() {
        try {
            valid();
            return Bundles.getBoolean(mFragment.getArguments(), Galley.PICTURE_MULTI_CHOICE_MODE);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public void read(Context context, Action1<PictureList> action1) {
        mList.read(context, action1);
    }

    public PictureList list() {
        return mList;
    }

    public MultiSelector multiSelector() {
        return mMultiSelector;
    }

    public void result(Activity activity) {
        Intent intent = new Intent();
        intent.putParcelableArrayListExtra(Galley.PICTURE_CHOICE_LIST, mList.makeCheckedPictureList());
        activity.setResult(Activity.RESULT_OK, intent);
        activity.finish();
    }

    public void goCrop(Picture picture) {
        try {
            Intent intent = new Intent();
            intent.setData(picture.fromUri(mFragment.getActivity()));
            Crop.goResultCrop(mFragment.getActivity(), Crop.REQUEST_CROP, intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private void valid() throws Exception {
        if (mFragment == null) {
            throw new NullPointerException("Fragment Null");
        }
    }
}
