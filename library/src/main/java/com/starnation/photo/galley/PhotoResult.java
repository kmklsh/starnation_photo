package com.starnation.photo.galley;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.starnation.util.StringUtil;

/*
 * @author lsh
 * @since 2018. 3. 22.
*/
public abstract class PhotoResult extends BroadcastReceiver {

    public final static String ACTION = "com.starnation.photo.PictureReceiver";

    final static String PATH = "PATH";

    final static String WIDTH = "WIDTH";

    final static String HEIGHT = "HEIGHT";

    public abstract void onResult(String bitmapPath, int width, int height);

    final Intent mIntent;


    public PhotoResult(Intent intent) {
        mIntent = intent;
    }

    @Override
    public final void onReceive(Context context, Intent intent) {
        String name = intent.getAction();
        if (StringUtil.equals(name, ACTION) == true) {
            onResult(intent.getStringExtra(PATH), intent.getIntExtra(WIDTH, 0), intent.getIntExtra(HEIGHT, 0));
        }
    }

    public static void send(@NonNull Activity activity, String bitmapPath, int width, int height) {
        Intent intent = new Intent(ACTION);
        intent.putExtra(PATH, bitmapPath);
        intent.putExtra(WIDTH, width);
        intent.putExtra(HEIGHT, height);
        activity.sendBroadcast(intent);
    }
}
