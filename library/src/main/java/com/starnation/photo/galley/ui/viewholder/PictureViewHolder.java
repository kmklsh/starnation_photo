package com.starnation.photo.galley.ui.viewholder;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.rebound.SimpleSpringListener;
import com.facebook.rebound.Spring;
import com.facebook.rebound.SpringSystem;
import com.starnation.android.os.Bundles;
import com.starnation.android.view.ViewUtil;
import com.starnation.photo.R;
import com.starnation.photo.galley.Galley;
import com.starnation.photo.galley.model.Picture;
import com.starnation.photo.galley.ui.widget.OnViewHolderEventListener;
import com.starnation.photo.galley.ui.widget.PictureImageView;
import com.starnation.widget.recyclerview.holder.SelectorViewHolder;

/*
 * @author lsh
 * @since 2017. 3. 21.
*/
public final class PictureViewHolder extends SelectorViewHolder<Picture> {

    //======================================================================
    // Variables
    //======================================================================

    PictureImageView mImageView;

    AppCompatImageView mCheckBox;

    CardView mLayoutCheckbox;

    Spring mSpring;

    final OnViewHolderEventListener mListener;

    //======================================================================
    // Constructor
    //======================================================================

    public PictureViewHolder(@NonNull ViewGroup viewGroup, OnViewHolderEventListener listener) {
        super(viewGroup, R.layout.viewholder_picture);
        mListener = listener;

        mLayoutCheckbox = (CardView) findViewById(R.id.layout_checkbox);
        mCheckBox = (AppCompatImageView) findViewById(R.id.checkbox);
        mCheckBox.setOnClickListener(createOnClickListener());

        mImageView = (PictureImageView) findViewById(R.id.imageview);
        mImageView.glideLoader().skipMemory(true);
        mImageView.glideLoader().defaultImage(R.drawable.grey_200);
        mImageView.setOnClickListener(createOnClickListener());

        mSpring = SpringSystem.create().createSpring();
        mSpring.addListener(new SimpleSpringListener() {
            @Override
            public void onSpringUpdate(Spring spring) {
                super.onSpringUpdate(spring);
                try {
                    float value = (float) spring.getCurrentValue();
                    mImageView.setScaleX(value);
                    mImageView.setScaleY(value);
                } catch (Exception ignored) {
                    // Nothing
                }
            }
        });
    }

    private View.OnClickListener createOnClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Picture picture = getItem();

                if (picture == null) {
                    return;
                }

                picture.toggle();
                boolean checked = picture.isChecked();
                if (Bundles.getBoolean(getArguments(), Galley.PICTURE_MULTI_CHOICE_MODE) == true) {
                    setChecked(checked);
                }
                if (mListener != null) {
                    mListener.onViewHolderEvent(PictureViewHolder.this, v.getId());
                }
            }
        };
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    protected void onBindArguments(@NonNull Bundle arguments) {
        super.onBindArguments(arguments);
        if (Bundles.getBoolean(arguments, Galley.PICTURE_MULTI_CHOICE_MODE) == true) {
            ViewUtil.show(mLayoutCheckbox);
        } else {
            ViewUtil.hide(mLayoutCheckbox);
        }
    }

    @Override
    public void onRefresh(Picture picture) {
        super.onRefresh(picture);
        mImageView.setRatio(1.0f);
        if (picture != null) {
            refreshCheckedBackground(picture.isChecked());
            scaleImageView(picture.isChecked());

            String url = picture.getThumbnailsPath();
            mImageView.setImageUrl(url);
        }
    }

    @Override
    public void release() {
        super.release();
        if (mImageView != null){
            mImageView.releaseImage();
        }
    }

    @Override
    public void onChecked(boolean checked) {
        super.onChecked(checked);
        scaleImageView(checked);
        setItemChecked(checked);
        refreshCheckedBackground(checked);
    }

    @Override
    public void setChecked(boolean checked) {
        super.setChecked(checked);
        scaleImageView(checked);
        setItemChecked(checked);
        refreshCheckedBackground(checked);
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private void setItemChecked(boolean checked) {
        Picture address = getItem();
        if (address != null) {
            address.setChecked(checked);
        }
    }

    private void scaleImageView(boolean checked) {
        final float maxScale = 1.0f;
        final float minScale = 0.8f;
        if (checked == true) {
            actionSpring(maxScale, minScale);
        } else {
            actionSpring(maxScale, maxScale);
        }
    }

    private void refreshCheckedBackground(boolean checked) {
        if (checked == true) {
            mLayoutCheckbox.setCardBackgroundColor(Util.getColorAccent(getContext()));
        } else {
            mLayoutCheckbox.setCardBackgroundColor(ContextCompat.getColor(getContext(), R.color.grey_400));
        }
    }

    private void actionSpring(float currentValue, float endValue) {
        mImageView.setScaleX(currentValue);
        mImageView.setScaleY(currentValue);
        mSpring.setCurrentValue(currentValue);
        mSpring.setEndValue(endValue);
    }
}
