package com.starnation.photo.galley.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.starnation.photo.R;
import com.starnation.photo.galley.Galley;
import com.starnation.photo.galley.model.Picture;
import com.starnation.photo.galley.model.PictureList;
import com.starnation.photo.galley.ui.viewholder.CaptureViewHolder;
import com.starnation.photo.galley.ui.viewholder.PictureViewHolder;
import com.starnation.widget.recyclerview.RecyclerViewAdapter;
import com.starnation.widget.recyclerview.RecyclerViewHolder;

import rx.functions.Action1;

/*
 * @author lsh
 * @since 2017. 3. 21.
*/
public class PictureListFragment extends RecyclerViewFragment {

    //======================================================================
    // Constants
    //======================================================================

    private final static int ITEM_PICTURE = 99;

    private final static int ITEM_PICK = 98;

    //======================================================================
    // Variables
    //======================================================================

    PictureListPresenter mPresenter;

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new PictureListPresenter(this);
        mPresenter.onCreate();

        setHasOptionsMenu(mPresenter.isMultiChoiceMode());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mPresenter != null) {
            mPresenter.onDestroy();
            mPresenter = null;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_picture_list, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_ok) {
            if (mPresenter != null) {
                if (mPresenter.isMultiChoiceMode() == true){
                    mPresenter.result(getActivity());
                }
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public RecyclerView.LayoutManager onCreateLayoutManager() {
        return new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
    }

    @Override
    public RecyclerView.Adapter onCreateRecyclerViewAdapter() {
        return new RecyclerViewAdapter() {
            @Override
            public int getItemHeaderCount() {
                return 1;
            }

            @Override
            public int getSupportItemCount() {
                try {
                    return mPresenter.list().size();
                } catch (Exception e) {
                    // Nothing
                }
                return 0;
            }

            @Override
            public int getSupportItemViewType(int position) {
                if (position < getItemHeaderCount()) {
                    return ITEM_PICK;
                }
                return ITEM_PICTURE;
            }

            @Override
            public Object getSupportItem(int viewType, int position) {
                try {
                    return mPresenter.list().get(position);
                } catch (Exception e) {
                    // Nothing
                }
                return null;
            }

            @Override
            protected void onBindArguments(int viewType, @NonNull Bundle arguments) {
                super.onBindArguments(viewType, arguments);
                if (mPresenter != null) {
                    arguments.putBoolean(Galley.PICTURE_MULTI_CHOICE_MODE, mPresenter.isMultiChoiceMode());
                }
            }

            @Override
            public RecyclerViewHolder onCreateItemViewHolder(ViewGroup parent, int viewType) {
                switch(viewType) {
                    case ITEM_PICK:
                        return new CaptureViewHolder(parent);
                }
                PictureViewHolder holder = new PictureViewHolder(parent, PictureListFragment.this);
                if (mPresenter != null) {
                    holder.setMultiSelector(mPresenter.multiSelector());
                }
                return holder;
            }
        };
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadPictureList();
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (mPresenter != null && mPresenter.isMultiChoiceMode() == true) {
            refreshTitle();
        }
    }

    @Override
    public void onViewHolderEvent(RecyclerViewHolder holder, int id) {
        if (mPresenter != null && mPresenter.isMultiChoiceMode() == false) {
            Object item = holder.getItem();
            if (item instanceof Picture){
                mPresenter.goCrop((Picture) item);
            }
        } else {
            refreshTitle();
        }
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private void refreshTitle() {
        String title;
        int count = mPresenter != null ? mPresenter.list().checkedItemCount() : 0;
        if (count > 0) {
            title = getString(R.string.photo_format_galley_name, count);
        } else {
            title = getString(R.string.galley_name);
        }
        setTitle(title);
    }

    private void loadPictureList() {
        try {
            mPresenter.read(getContext(), new Action1<PictureList>() {
                @Override
                public void call(PictureList pictureList) {
                    getRecyclerView().getAdapter().notifyDataSetChanged();
                }
            });
        } catch (Exception e) {
            // Nothing
        }
    }
}
