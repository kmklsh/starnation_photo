package com.starnation.photo.galley.ui.viewholder;

import android.content.Context;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.util.TypedValue;

import com.starnation.photo.R;

/*
 * @author lsh
 * @since 2017. 3. 23.
*/
final class Util {

    @ColorInt
    public static int getColorAccent(@NonNull Context context) {
        TypedValue value = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.colorAccent, value, true);
        return value.data;
    }
}
