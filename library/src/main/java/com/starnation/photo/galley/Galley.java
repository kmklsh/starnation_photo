package com.starnation.photo.galley;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.starnation.photo.galley.model.Picture;

import java.util.ArrayList;

/*
 * @author lsh
 * @since 2017. 3. 22.
*/
public final class Galley {

    //======================================================================
    // Constants
    //======================================================================

    public static final int REQUEST_PICTURE = 10001;

    public final static String PICTURE_MULTI_CHOICE_MODE = "PICTURE_MULTI_CHOICE_MODE";

    public final static String PICTURE_CHOICE_LIST = "PICTURE_CHOICE_LIST";

    //======================================================================
    // Public Methods
    //======================================================================

    public static void goPicture(@NonNull Activity context, boolean multiChoiceMode) {
        Intent intent = new Intent(context, GalleyActivity.class);
        intent.putExtra(PICTURE_MULTI_CHOICE_MODE, multiChoiceMode);
        context.startActivityForResult(intent, REQUEST_PICTURE);
    }

    public static ArrayList<Picture> getCheckedPictureList(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_PICTURE && resultCode == Activity.RESULT_OK) {
            return data.getParcelableArrayListExtra(PICTURE_CHOICE_LIST);
        }
        return null;
    }
}
