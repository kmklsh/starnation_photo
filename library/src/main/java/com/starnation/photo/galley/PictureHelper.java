package com.starnation.photo.galley;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;

import com.starnation.android.content.IntentFactory;
import com.starnation.android.graphics.bitmap.BitmapCache;
import com.starnation.android.util.PermissionChecker;
import com.starnation.photo.crop.Crop;
import com.starnation.photo.crop.model.CropResult;
import com.starnation.photo.galley.model.Picture;
import com.starnation.util.validator.CollectionValidator;
import com.starnation.util.validator.InvalidException;

import java.util.ArrayList;
import java.util.Locale;

/*
 * @author lsh
 * @since 2018. 3. 22.
*/
public final class PictureHelper {

    //======================================================================
    // Constants
    //======================================================================

    private final static String PHOTO_DIRECTORY = "PictureHelper";

    private final static String PHOTO_FILE_NAME = "Picture%d.png";

    //======================================================================
    // Variables
    //======================================================================

    Activity mActivity;

    PermissionChecker mPermissionChecker;

    //======================================================================
    // Constructor
    //======================================================================

    public PictureHelper(@NonNull Activity activity, PermissionChecker checker) {
        mActivity = activity;
        mPermissionChecker = checker;
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public void onDestroy() {
        mActivity = null;
        mPermissionChecker = null;
    }

    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        switch(requestCode) {
            case Crop.REQUEST_CAPTURE:
            case Crop.REQUEST_PICK:
                if (resultCode == Activity.RESULT_OK) {
                    Crop.goResultCrop(mActivity, Crop.REQUEST_CROP, data);
                }
                break;
            case Galley.REQUEST_PICTURE:
                resultRequestPicture(requestCode, resultCode, data);
                break;
            case Crop.REQUEST_CROP:
                if (resultCode == Activity.RESULT_OK) {
                    goCropResult(Crop.getCropBitmap(mActivity));
                }
                break;
        }
    }

    public void goPicture() {
        shouldShowRequestPermissionRationale();
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private static ArrayList<String> makeRequestPermissions(@NonNull Activity activity) {
        ArrayList<String> list = new ArrayList<>();
        if (isPermissionGranted(activity, Manifest.permission.CAMERA) == false) {
            list.add(Manifest.permission.CAMERA);
        }
        if (isPermissionGranted(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) == false) {
            list.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        return list;
    }

    private static boolean isAllChecked(boolean[] array) {
        for (boolean b : array) {
            if (b == false) {
                return false;
            }
        }
        return true;
    }

    /**
     * 권환획득 여부 체크
     *
     * @param activity   {@link Activity}
     * @param permission {@link Manifest.permission} , {@link Manifest.permission} 에서 정의된 값 사용 권장
     *
     * @return 권환획득이면 true 아니면 false
     */
    private static boolean isPermissionGranted(@NonNull Activity activity, @NonNull String permission) {
        return ContextCompat.checkSelfPermission(activity, permission) == PackageManager.PERMISSION_GRANTED;
    }

    private boolean shouldShowRequestPermissionRationale() {
        mPermissionChecker.makeRequestPermissions(
                makeRequestPermissions(mActivity),
                new PermissionChecker.OnSupportRequestPermissionsResultCallback() {
                    @Override
                    public void onResult(boolean[] grants, boolean[] showRequestPermissions) {
                        boolean grant = isAllChecked(grants);
                        boolean showRequestPermission = isAllChecked(showRequestPermissions);
                        if (grant == true) {
                            Crop.goResultCapture(mActivity, mActivity.getPackageName() + ".fileprovider");
                        } else if (showRequestPermission == true) {
                            IntentFactory.goApplicationDetailsSetting(mActivity);
                        }
                    }
                });
        return true;
    }

    private void resultRequestPicture(int requestCode, int resultCode, Intent data) {
        try {
            if (resultCode != Activity.RESULT_OK) {
                throw new InvalidException("not resultCode Activity.RESULT_OK");
            }

            ArrayList<Picture> list = Galley.getCheckedPictureList(requestCode, resultCode, data);

            if (CollectionValidator.isValid(list) == true) {
                Picture picture = list.get(0);
                Intent intent = new Intent();
                intent.setData(picture.fromUri(mActivity));

                Crop.goResultCrop(mActivity, Crop.REQUEST_CROP, intent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void goCropResult(Bitmap bitmap){
        CropResult result = null;
        if (bitmap != null){
            BitmapCache bitmapCache = BitmapCache.create(
                    mActivity,
                    PHOTO_DIRECTORY,
                    String.format(Locale.US, PHOTO_FILE_NAME, System.currentTimeMillis()));
            bitmapCache.delete();
            bitmapCache.writeJPEGFile(bitmap);

            result = new CropResult(bitmapCache.getCacheFile().getAbsolutePath(), bitmap.getWidth(), bitmap.getHeight());
            bitmap.recycle();
        }
        Intent intent = new Intent();
        intent.putExtra(Crop.KEY_CROP_RESULT, result);
        mActivity.setResult(Activity.RESULT_OK, intent);
        mActivity.finish();
    }
}
