package com.starnation.photo.galley.model;

import android.app.Activity;
import android.content.ContentUris;
import android.database.Cursor;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.widget.Checkable;

/*
 * @author lsh
 * @since 2017. 3. 21.
*/
public final class Picture implements Checkable, Parcelable {

    //======================================================================
    // Variables
    //======================================================================

    String mPath;

    int mOrientation;

    boolean mChecked;

    String mThumbnailsPath;

    //======================================================================
    // Constructor
    //======================================================================

    public Picture(String thumbnailsPath, String path, int orientation) {
        mThumbnailsPath = thumbnailsPath;
        mPath = path;
        mOrientation = orientation;
    }

    protected Picture(Parcel in) {
        this.mThumbnailsPath = in.readString();
        this.mPath = in.readString();
        this.mOrientation = in.readInt();
        this.mChecked = in.readByte() != 0;
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public String getThumbnailsPath() {
        return mThumbnailsPath;
    }

    public String getPath() {
        return mPath;
    }

    public int getOrientation() {
        return mOrientation;
    }

    public Uri fromUri(Activity activity) {
        Uri resultUri = null;
        Uri fileUri = Uri.parse(mPath);
        String filePath = fileUri.getPath();
        Cursor c = activity.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null, "_data = '" + filePath + "'", null, null);
        if (c != null) {
            c.moveToNext();
            int id = c.getInt(c.getColumnIndex("_id"));
            resultUri = ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, id);
            c.close();
        }
        return resultUri;
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mThumbnailsPath);
        dest.writeString(this.mPath);
        dest.writeInt(this.mOrientation);
        dest.writeByte(this.mChecked ? (byte) 1 : (byte) 0);
    }

    @Override
    public void setChecked(boolean checked) {
        mChecked = checked;
    }

    @Override
    public boolean isChecked() {
        return mChecked;
    }

    @Override
    public void toggle() {
        setChecked(!mChecked);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    //======================================================================
    // CREATOR
    //======================================================================

    public static final Parcelable.Creator<Picture> CREATOR = new Parcelable.Creator<Picture>() {
        @Override
        public Picture createFromParcel(Parcel source) {
            return new Picture(source);
        }

        @Override
        public Picture[] newArray(int size) {
            return new Picture[size];
        }
    };
}
