package com.starnation.photo.galley.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Checkable;

/*
 * @author lsh
 * @since 2017. 3. 23.
*/
public class CheckedImageView extends PictureImageView implements Checkable {

    //======================================================================
    // Constants
    //======================================================================

    public static final int[] CHECKED_STATE = {android.R.attr.state_checked};

    //======================================================================
    // Variables
    //======================================================================

    private boolean mChecked;

    private View.OnClickListener mOnClickListener;

    //======================================================================
    // Constructor
    //======================================================================

    public CheckedImageView(Context context) {
        super(context);
        init();
    }

    public CheckedImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CheckedImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        super.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggle();
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);
                }
            }
        });
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public void setChecked(boolean checked) {
        mChecked = checked;
        refreshDrawableState();
    }

    @Override
    public boolean isChecked() {
        return mChecked;
    }

    @Override
    public void toggle() {
        setChecked(!mChecked);
    }

    @Override
    public int[] onCreateDrawableState(int extraSpace) {
        int[] drawableState = super.onCreateDrawableState(extraSpace + 1);
        if (isChecked()) {
            mergeDrawableStates(drawableState, CHECKED_STATE);
        }
        return drawableState;
    }

    @Override
    protected void drawableStateChanged() {
        super.drawableStateChanged();
        invalidate();
    }

    @Override
    public void setOnClickListener(View.OnClickListener onClickListener) {
        mOnClickListener = onClickListener;
    }
}
