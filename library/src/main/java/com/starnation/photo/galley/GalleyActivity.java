package com.starnation.photo.galley;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.starnation.android.util.PermissionChecker;
import com.starnation.photo.R;
import com.starnation.photo.galley.fragment.PictureListFragment;

/*
 * @author lsh
 * @since 2017. 3. 21.
*/
public final class GalleyActivity extends AppCompatActivity {

    //======================================================================
    // Variables
    //======================================================================

    private PictureHelper mPictureHelper;

    private PermissionChecker mPermissionChecker;

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPermissionChecker = new PermissionChecker(this);
        mPictureHelper = new PictureHelper(this, mPermissionChecker);
        setContentView(R.layout.activity_galley);

        if (savedInstanceState == null) {
            Bundle bundle = new Bundle();
            bundle.putBoolean(Galley.PICTURE_MULTI_CHOICE_MODE, getIntent().getBooleanExtra(Galley.PICTURE_MULTI_CHOICE_MODE, false));

            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, Fragment.instantiate(GalleyActivity.this, PictureListFragment.class.getName(), bundle))
                    .commit();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (mPictureHelper != null) {
            mPictureHelper.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (mPermissionChecker != null) {
            mPermissionChecker.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mPictureHelper != null) {
            mPictureHelper.onDestroy();
            mPictureHelper = null;
        }

        if (mPermissionChecker != null) {
            mPermissionChecker.release();
            mPermissionChecker = null;
        }
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public PictureHelper getPictureHelper() {
        return mPictureHelper;
    }
}
