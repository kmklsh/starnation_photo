package com.starnation.photo.galley.model;

import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/*
 * @author lsh
 * @since 2017. 3. 21.
*/
public class PictureList {

    //======================================================================
    // Variables
    //======================================================================

    ArrayList<Picture> mList = new ArrayList<>();

    //======================================================================
    // Public Methods
    //======================================================================

    public static String getThumnailPath(Context context, String path) {
        String result = null;
        long imageId = -1;

        try {
            String[] projection = new String[]{MediaStore.MediaColumns._ID};
            String selection = MediaStore.MediaColumns.DATA + "=?";
            String[] selectionArgs = new String[]{path};
            Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                imageId = cursor.getInt(cursor.getColumnIndex(MediaStore.MediaColumns._ID));
            }
            cursor.close();

            cursor = MediaStore.Images.Thumbnails.queryMiniThumbnail(context.getContentResolver(), imageId, MediaStore.Images.Thumbnails.MINI_KIND, null);
            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();
                result = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Images.Thumbnails.DATA));
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (TextUtils.isEmpty(result))
            result = path;
        return result;
    }

    public void read(@NonNull Context context, final Action1<PictureList> action1) {
        Observable.just(context).map(new Func1<Context, ArrayList<Picture>>() {
            @Override
            public ArrayList<Picture> call(Context context) {
                ArrayList<Picture> list = new ArrayList<>();

                try {
                    String[] columns = {
                            MediaStore.Images.Thumbnails._ID,
                            MediaStore.Images.Thumbnails.DATA,
                            MediaStore.Images.Media.DATA,
                            MediaStore.Images.Media.ORIENTATION};

                    Cursor cursor = context.getContentResolver().query(
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                            columns,
                            null,
                            null,
                            MediaStore.Images.Media.DATE_ADDED + " DESC");
                    if (cursor != null) {
                        cursor.moveToFirst();

                        list.clear();
                        String path;
                        String thumbnailsPath;
                        int orientation;

                        int thumbnails = cursor.getColumnIndex(MediaStore.Images.Thumbnails.DATA);
                        int original = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
                        int orientationCol = cursor.getColumnIndex(MediaStore.Images.Media.ORIENTATION);
                        while (cursor.moveToNext()) {
                            thumbnailsPath = cursor.getString(thumbnails);
                            path = cursor.getString(original);
                            orientation = cursor.getInt(orientationCol);
                            list.add(new Picture(thumbnailsPath, path, orientation));
                        }
                    }
                    if (cursor != null) {
                        cursor.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return list;
            }
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(new Subscriber<ArrayList<Picture>>() {
                    @Override
                    public void onCompleted() {
                        // Nothing
                    }

                    @Override
                    public void onError(Throwable e) {
                        // Nothing
                    }

                    @Override
                    public void onNext(ArrayList<Picture> pictures) {
                        mList.clear();
                        mList.addAll(pictures);
                        if (action1 != null) {
                            action1.call(PictureList.this);
                        }
                    }
                });
    }

    public int size() {
        return mList.size();
    }

    public boolean contains(Picture o) {
        return mList.contains(o);
    }

    public boolean add(Picture o) {
        return mList.add(o);
    }

    public boolean remove(Picture o) {
        return mList.remove(o);
    }

    public boolean addAll(ArrayList<Picture> c) {
        return mList.addAll(c);
    }

    public boolean addAll(int index, ArrayList<Picture> c) {
        return mList.addAll(index, c);
    }

    public void clear() {
        mList.clear();
    }

    public Picture get(int index) {
        return mList.get(index);
    }

    public ArrayList<Picture> makeCheckedPictureList() {
        ArrayList<Picture> list = new ArrayList<>();
        for (Picture picture : mList) {
            if (picture.isChecked() == true) {
                list.add(picture);
            }
        }
        return list;
    }

    public int checkedItemCount() {
        int count = 0;
        for (Picture picture : mList) {
            if (picture.isChecked() == true) {
                count++;
            }
        }
        return count;
    }
}
