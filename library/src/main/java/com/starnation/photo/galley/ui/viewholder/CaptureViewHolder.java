package com.starnation.photo.galley.ui.viewholder;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;

import com.starnation.photo.R;
import com.starnation.photo.galley.GalleyActivity;
import com.starnation.photo.galley.ui.widget.PictureImageView;
import com.starnation.widget.recyclerview.RecyclerViewHolder;

/*
 * @author lsh
 * @since 2018. 3. 22.
*/
public final class CaptureViewHolder extends RecyclerViewHolder {

    //======================================================================
    // Variables
    //======================================================================

    PictureImageView mImageView;

    //======================================================================
    // Constructor
    //======================================================================

    public CaptureViewHolder(@NonNull ViewGroup viewGroup) {
        super(viewGroup, R.layout.viewholder_pick_picture);
        mImageView = (PictureImageView) findViewById(R.id.imageview);
        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getContext() instanceof GalleyActivity){
                    ((GalleyActivity) getContext()).getPictureHelper().goPicture();
                }
            }
        });
        mImageView.setRatio(1.0f);
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public void onRefresh(Object picture) {
        /*DrawableHelper
                .with(ContextCompat.getDrawable(getContext(), R.drawable.ic_photo_camera_black_24dp))
                .color(ContextCompat.getColor(getContext(), getColorSecondary(getContext())))
                .applyTo(mImageView);*/
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private static int getColorSecondary(Context context) {
        TypedValue value = new TypedValue();
        context.getTheme().resolveAttribute(android.R.attr.textColorSecondary, value, true);
        return value.data;
    }
}
