package com.starnation.photo.crop;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.opengl.GLES10;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.starnation.android.graphics.bitmap.BitmapCache;
import com.starnation.photo.R;

import java.io.File;

import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func0;
import rx.schedulers.Schedulers;

/*
 * @author lsh
 * @since 2016. 1. 7.
 */
class CropImage {

    //======================================================================
    // Constants
    //======================================================================

    static final String TEMP_FILE_NAME = "temp_photo.jpg";

    static final String TEMP_CROP_FILE_NAME = "temp_crop_photo.jpg";

    static final String DIRECTORY = "images";

    private static final int SIZE_DEFAULT = 1024;

    private static final int SIZE_LIMIT = 4096;

    //======================================================================
    // Variables
    //======================================================================

    private int mSampleSize;

    private Activity mActivity;

    private Builder mBuilder;

    private int mOrientation;

    //======================================================================
    // Constructor
    //======================================================================

    public CropImage(@NonNull Activity activity, Builder builder) {
        mActivity = activity;
        mBuilder = builder;
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public void load(final ImageView view, final Callback callback) {
        if (callback == null) {
            return;
        }

        Intent resultIntent = mActivity.getIntent().getParcelableExtra(Crop.Extra.RESULT_INTENT);
        final Uri uri = resultIntent != null ? resultIntent.getData() : null;

        mOrientation = CropUtil.getOrientation(mActivity, resultIntent, null);


        BitmapCache cache = BitmapCache.create(mActivity, DIRECTORY, TEMP_FILE_NAME);
        Bitmap bitmap;
        bitmap = cache.getBitmap();
        if (bitmap != null) {
            //noinspection unchecked
            callback.onCallback(new BitmapDrawable(mActivity.getResources(), bitmap));
        } else {
            Glide.with(view)
                    .load(uri)
                    .apply(new RequestOptions()
                            .disallowHardwareConfig()
                            .format(DecodeFormat.PREFER_ARGB_8888)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .placeholder(R.drawable.drawable_translate_image)
                            .skipMemoryCache(true))
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            callback.onCallback(resource);
                            return true;
                        }
                    })
                    .into(view);
        }
    }

    public void saveCrop(@NonNull final Bitmap src, @NonNull final RectF crop, final Callback<Boolean> callback) {
        new AsyncLoad<Boolean>() {
            @SuppressWarnings({"ConstantConditions", "ResultOfMethodCallIgnored"})
            @Override
            public Boolean onBackground() {
                Rect rect = new Rect((int) crop.left, (int) crop.top, (int) crop.right, (int) crop.bottom);
                Bitmap bitmap = Bitmap.createBitmap(src, rect.left, rect.top, rect.width(), rect.height());
                BitmapCache cache = BitmapCache.create(mActivity, DIRECTORY, TEMP_CROP_FILE_NAME);
                if (bitmap != null) {
                    cache.writeJPEGFile(bitmap);
                } else {
                    cache.delete();
                }
                return bitmap != null;
            }

            @Override
            public void onComplete(Boolean aBoolean) {
                boolean result = aBoolean;
                if (callback != null) {
                    callback.onCallback(result);
                }
            }
        }.start(mActivity);
    }

    public void saveCrop(@NonNull final Drawable src, @NonNull final RectF crop, final Callback<Boolean> callback) {
        new AsyncLoad<Boolean>() {
            @SuppressWarnings({"ConstantConditions", "ResultOfMethodCallIgnored"})
            @Override
            public Boolean onBackground() {
                Rect rect = new Rect((int) crop.left, (int) crop.top, (int) crop.right, (int) crop.bottom);
                if (src != null) {
                    Bitmap bitmap = CropUtil.createBitmap(src, rect);
                    BitmapCache cache = BitmapCache.create(mActivity, DIRECTORY, TEMP_CROP_FILE_NAME);
                    if (bitmap != null) {
                        cache.writeJPEGFile(bitmap);
                    } else {
                        cache.delete();
                    }
                    return bitmap != null;
                }
                return false;
            }

            @Override
            public void onComplete(Boolean aBoolean) {
                boolean result = aBoolean;
                if (callback != null) {
                    callback.onCallback(result);
                }
            }
        }.start(mActivity);
    }

    public int getOrientation() {
        return mOrientation;
    }

    public int getSampleSize() {
        return mSampleSize;
    }

    public int getAspectX() {
        return mBuilder.aspectX;
    }

    public int getAspectY() {
        return mBuilder.aspectY;
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public void release() {
        File file = BitmapCache.createCacheFile(mActivity, DIRECTORY, TEMP_FILE_NAME);
        if (file != null) {
            file.delete();
        }

        mActivity = null;
        mBuilder = null;
    }

    //======================================================================
    // Private Methods
    //======================================================================

    public static int getMaxImageSize() {
        int textureLimit = getMaxTextureSize();
        if (textureLimit == 0) {
            return SIZE_DEFAULT;
        } else {
            return Math.min(textureLimit, SIZE_LIMIT);
        }
    }

    private static int getMaxTextureSize() {
        int[] maxSize = new int[1];
        GLES10.glGetIntegerv(GLES10.GL_MAX_TEXTURE_SIZE, maxSize, 0);
        return maxSize[0];
    }

    //======================================================================
    // Callback
    //======================================================================

    public interface Callback<Result> {
        void onCallback(Result result);
    }

    //======================================================================
    // Builder
    //======================================================================

    public final static class Builder {

        int aspectX;

        int aspectY;

        int maxX = getMaxImageSize();

        int maxY = getMaxImageSize();

        Uri sourceUri;

        public Builder setAspectX(int aspectX) {
            this.aspectX = aspectX;
            return this;
        }

        public Builder setAspectY(int aspectY) {
            this.aspectY = aspectY;
            return this;
        }

        public Builder setMaxX(int maxX) {
            if (maxX > 0) {
                this.maxX = maxX;
            }
            return this;
        }

        public Builder setMaxY(int maxY) {
            if (maxY > 0) {
                this.maxY = maxY;
            }
            return this;
        }

        public Builder setSourceUri(Uri sourceUri) {
            this.sourceUri = sourceUri;
            return this;
        }

        public CropImage build(@NonNull Activity activity) {
            return new CropImage(activity, this);
        }
    }

    //======================================================================
    // AsyncLoad
    //======================================================================

    abstract static class AsyncLoad<Result> {

        private Thread mThread;

        private ProgressDialog mProgressDialog;

        private final Handler mHandler = new Handler(Looper.getMainLooper());

        public void start(@NonNull Activity activity) {
       /*     if (mThread != null) {
                mThread.interrupt();
                mThread = null;
            }*/
            onStopProgress();

            /* Not Formatter */
            mProgressDialog = ProgressDialog.show(
                    activity,
                    null,
                    activity.getResources().getString(R.string.crop_wait),
                    true,
                    false);


            Observable.defer(new Func0<Observable<Result>>() {
                @Override
                public Observable<Result> call() {
                    return Observable.just(onBackground());
                }
            }).observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.newThread())
                    .subscribe(new Observer<Result>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            onStopProgress();
                            onComplete(null);
                            release();
                        }

                        @Override
                        public void onNext(Result result) {
                            onStopProgress();
                            onComplete(result);
                            release();
                        }
                    });
        }

        public abstract Result onBackground();

        public abstract void onComplete(Result result);

        void onStopProgress() {
            try {
                if (mProgressDialog != null) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }
            } catch (Exception ignored) {
            }
        }

        void release() {
            if (mThread != null) {
                mThread.interrupt();
                mThread = null;
            }
            onStopProgress();
        }
    }

}
