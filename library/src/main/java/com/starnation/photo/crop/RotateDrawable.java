/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.starnation.photo.crop;

import android.graphics.Matrix;
import android.graphics.drawable.Drawable;

/*
 * Modified from original in AOSP.
 */
class RotateDrawable {

    private Drawable bitmap;
    private int rotation;

    public RotateDrawable(Drawable bitmap, int rotation) {
        this.bitmap = bitmap;
        this.rotation = rotation % 360;
    }

    @Deprecated
    public void setRotation(int rotation) {
        this.rotation = rotation;
    }

    @Deprecated
    public int getRotation() {
        return rotation;
    }

    public Drawable getBitmap() {
        return bitmap;
    }

    public void setBitmap(Drawable bitmap) {
        this.bitmap = bitmap;
    }

    @Deprecated
    public Matrix getRotateMatrix() {
        // By default this is an identity matrix
        Matrix matrix = new Matrix();
        if (bitmap != null && rotation != 0) {
            // We want to do the rotation at origin, but since the bounding
            // rectangle will be changed after rotation, so the delta values
            // are based on old & new width/height respectively.
            int cx = bitmap.getIntrinsicWidth() / 2;
            int cy = bitmap.getIntrinsicHeight() / 2;
            matrix.preTranslate(-cx, -cy);
            matrix.postRotate(rotation);
            matrix.postTranslate(getWidth() / 2, getHeight() / 2);
        }
        return matrix;
    }

    @Deprecated
    public boolean isOrientationChanged() {
        return /*(rotation / 90) % 2 != 0*/ false;
    }

    public int getHeight() {
        if (bitmap == null) return 0;
        if (isOrientationChanged()) {
            return bitmap.getIntrinsicWidth();
        } else {
            return bitmap.getIntrinsicHeight();
        }
    }

    public int getWidth() {
        if (bitmap == null) return 0;
        if (isOrientationChanged()) {
            return bitmap.getIntrinsicHeight();
        } else {
            return bitmap.getIntrinsicWidth();
        }
    }

    public void recycle() {
        if (bitmap != null) {
            bitmap = null;
        }
    }
}

