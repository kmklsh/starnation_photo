package com.starnation.photo.crop.model;

import android.os.Parcel;
import android.os.Parcelable;

/*
 * @author lsh
 * @since 2018. 3. 22.
*/
public final class CropResult implements Parcelable {

    String mPath;

    int mWidth;

    int mHeight;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mPath);
        dest.writeInt(this.mWidth);
        dest.writeInt(this.mHeight);
    }

    public CropResult(String path, int width, int height) {
        mPath = path;
        mWidth = width;
        mHeight = height;
    }

    public CropResult() {
    }

    protected CropResult(Parcel in) {
        this.mPath = in.readString();
        this.mWidth = in.readInt();
        this.mHeight = in.readInt();
    }

    public String getPath() {
        return mPath;
    }

    public int getWidth() {
        return mWidth;
    }

    public int getHeight() {
        return mHeight;
    }

    public static final Creator<CropResult> CREATOR = new Creator<CropResult>() {
        @Override
        public CropResult createFromParcel(Parcel source) {
            return new CropResult(source);
        }

        @Override
        public CropResult[] newArray(int size) {
            return new CropResult[size];
        }
    };
}
