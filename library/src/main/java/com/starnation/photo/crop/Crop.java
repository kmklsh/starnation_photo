package com.starnation.photo.crop;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;

import com.starnation.android.graphics.bitmap.BitmapCache;
import com.starnation.photo.crop.model.CropResult;

/**
 * Builder for crop Intents and utils for handling result
 */
public final class Crop {

    public static final int REQUEST_CROP = 6709;
    public static final int REQUEST_PICK = 9162;
    public static final int REQUEST_CAPTURE = 9164;

    public final static String KEY_CROP_RESULT = "KEY_CROP_RESULT";

    interface Extra {
        String ASPECT_X = "aspect_x";
        String ASPECT_Y = "aspect_y";
        String MAX_X = "max_x";
        String MAX_Y = "max_y";
        String RESULT_INTENT = "RESULT_INTENT";
        String CROP_OUTPUT = "CROP_OUTPUT";
    }

    public static void goResultPick(@NonNull Activity activity) {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        intent.setData(MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(BitmapCache.createCacheFile(activity, CropImage.DIRECTORY, CropImage.TEMP_FILE_NAME)));
        activity.startActivityForResult(intent, REQUEST_PICK);
    }

    public static void goResultCapture(@NonNull Activity activity, String authority) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Uri uri = FileProvider.getUriForFile(activity, authority, BitmapCache.createCacheFile(activity, CropImage.DIRECTORY, CropImage.TEMP_FILE_NAME));
            intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            activity.startActivityForResult(intent, REQUEST_CAPTURE);
        } else {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(BitmapCache.createCacheFile(activity, CropImage.DIRECTORY, CropImage.TEMP_FILE_NAME)));
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            activity.startActivityForResult(intent, REQUEST_CAPTURE);
        }
    }

    public static void goResultCrop(@NonNull Activity activity, int requestCode, Intent resultIntent) {
        Intent intent = new Intent(activity, CropImageActivity.class);
        intent.putExtra(Extra.RESULT_INTENT, resultIntent);
        activity.startActivityForResult(intent, requestCode);
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public static Bitmap getCropBitmap(@NonNull Context context) {
        BitmapCache cache = BitmapCache.create(context, CropImage.DIRECTORY, CropImage.TEMP_CROP_FILE_NAME);
        if (cache != null) {
            Bitmap bitmap = cache.getBitmap();
            cache.delete();
            return bitmap;
        }
        return null;
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public static Bitmap getResultBitmap(@NonNull Activity context, Intent resultIntent) {
        BitmapCache cache = BitmapCache.create(context, CropImage.DIRECTORY, CropImage.TEMP_FILE_NAME);
        if (cache != null) {
            try {
                Bitmap bitmap = CropUtil.getResultBitmap(context, resultIntent, CropImage.getMaxImageSize(), cache.getCacheFile());
                cache.delete();
                return bitmap;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static CropResult getCropResult(Intent resultIntent) {
        if (resultIntent != null) {
            return resultIntent.getParcelableExtra(Crop.KEY_CROP_RESULT);
        }
        return null;
    }
}
