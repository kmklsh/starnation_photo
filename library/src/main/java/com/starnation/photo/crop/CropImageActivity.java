/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.starnation.photo.crop;

import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.starnation.photo.R;

/*
 * Modified from original in AOSP.
 */
public class CropImageActivity extends MonitoredActivity {

    private CropImageView imageView;
    private HighlightView cropView;

    private CropImage mCropImage;

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setupWindowFlags();
        setupViews();

        init();
        if (mCropImage == null) {
            finish();
            return;
        }
        startCrop();
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private void setupWindowFlags() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }

    private void init() {
        Intent intent = getIntent();
        if (intent != null) {
            mCropImage = new CropImage.Builder()
                    .setAspectX(intent.getIntExtra(Crop.Extra.ASPECT_X, 0))
                    .setAspectY(intent.getIntExtra(Crop.Extra.ASPECT_Y, 0))
                    .setMaxX(intent.getIntExtra(Crop.Extra.MAX_X, 0))
                    .setMaxY(intent.getIntExtra(Crop.Extra.MAX_Y, 0))
                    .setSourceUri(intent.getData())
                    .build(this);

        }
    }

    private void setupViews() {
        setContentView(R.layout.crop_activity_crop);

        imageView = findViewById(R.id.crop_image);
        /*imageView.setScaleType(ImageView.ScaleType.MATRIX);*/
        imageView.context = this;
        imageView.setRecycler(new ImageViewTouchBase.Recycler() {
            @Override
            public void recycle(Bitmap b) {
                b.recycle();
                System.gc();
            }
        });

        findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        findViewById(R.id.btn_done).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (imageView.highlightViews.size() > 0) {
                    mCropImage.saveCrop(imageView.getBitmap(), imageView.getCropRect(), new CropImage.Callback<Boolean>() {
                        @Override
                        public void onCallback(Boolean result) {
                            setResult(RESULT_OK, new Intent().putExtra(Crop.Extra.CROP_OUTPUT, result == true));
                            finish();
                        }
                    });
                }
            }
        });
    }

    private void startCrop() {
        mCropImage.load(imageView, new CropImage.Callback<Drawable>() {
            @Override
            public void onCallback(Drawable drawable) {
                RotateDrawable rotateDrawable = new RotateDrawable(drawable, mCropImage.getOrientation());
                imageView.setImageRotateBitmapResetBase(rotateDrawable, true);
                if (imageView.getScale() == 1F) {
                    imageView.center();
                }

                imageView.add(HighlightView.create(
                        imageView,
                        rotateDrawable.getWidth(),
                        rotateDrawable.getHeight(),
                        mCropImage.getAspectX(),
                        mCropImage.getAspectY(),
                        imageView.getUnrotatedMatrix()));
                imageView.invalidate();

                if (imageView.highlightViews.size() == 1) {
                    cropView = imageView.highlightViews.get(0);
                    cropView.setFocus(true);
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCropImage != null) {
            mCropImage.release();
            mCropImage = null;
        }
        cropView = null;
    }
}
