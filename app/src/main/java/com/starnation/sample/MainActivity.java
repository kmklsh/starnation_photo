package com.starnation.sample;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.view.ViewGroup;

import com.starnation.android.util.Logger;
import com.starnation.photo.crop.Crop;
import com.starnation.photo.crop.model.CropResult;
import com.starnation.photo.galley.Galley;
import com.starnation.photo.galley.model.Picture;
import com.starnation.photo.galley.ui.viewholder.PictureViewHolder;
import com.starnation.photo.galley.ui.widget.OnViewHolderEventListener;
import com.starnation.util.validator.CollectionValidator;
import com.starnation.widget.recyclerview.RecyclerViewAdapter;
import com.starnation.widget.recyclerview.RecyclerViewHolder;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    RecyclerView mTextView;

    ArrayList<Picture> mList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Galley.goPicture(MainActivity.this, false);
            }
        });

        mTextView = (RecyclerView) findViewById(R.id.recyclerview);
        mTextView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));

        mTextView.setAdapter(new RecyclerViewAdapter() {
            @Override
            public int getItemHeaderCount() {
                return 0;
            }

            @Override
            public int getSupportItemCount() {
                return CollectionValidator.getSize(mList);
            }

            @Override
            public Object getSupportItem(int viewType, int position) {
                if (CollectionValidator.isValid(mList, position) == true) {
                    return mList.get(position);
                }
                return null;
            }

            @Override
            public RecyclerViewHolder onCreateItemViewHolder(ViewGroup parent, int viewType) {
                return new PictureViewHolder(parent, new OnViewHolderEventListener() {
                    @Override
                    public void onViewHolderEvent(RecyclerViewHolder holder, int id) {

                    }
                });
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ArrayList<Picture> list = Galley.getCheckedPictureList(requestCode, resultCode, data);
        if (list != null) {
            StringBuilder builder = new StringBuilder();
            for (Picture picture : list) {
                builder.append(picture.getPath()).append("_").append(Integer.toString(picture.getOrientation())).append("\n");
            }

            mList.clear();
            mList.addAll(list);

            mTextView.getAdapter().notifyDataSetChanged();

            /*Picture picture = list.get(0);
            Intent intent = new Intent();
            intent.setData(picture.fromUri(this));
            Crop.goResultCrop(this, Crop.REQUEST_CROP, intent);*/
        } else {
            CropResult result = Crop.getCropResult(data);
            if (result != null) {
                mList.clear();

                Picture picture = new Picture(result.getPath(), result.getPath(), 0);
                mList.add(picture);
                mTextView.getAdapter().notifyDataSetChanged();

                Logger.d("CROP", "PATH : " + result.getPath() + " width : " + result.getWidth());
            }
        }
    }
}
