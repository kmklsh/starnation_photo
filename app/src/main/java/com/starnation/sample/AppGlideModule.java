package com.starnation.sample;

import android.content.Context;

import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.annotation.GlideModule;
import com.starnation.glide.module.DefaultAppGlideModule;

/*
 * kotlin 변환하면 glide에서 인식을 못한다.
 * @author lsh
 * @since 2018. 7. 13.
 */
@GlideModule
public class AppGlideModule extends DefaultAppGlideModule {
    @Override
    public void applyOptions(Context context, GlideBuilder builder) {
        super.applyOptions(context, builder);
    }
}
